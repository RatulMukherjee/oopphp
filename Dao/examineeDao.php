<?php
    require_once('baseDao.php');
    class ExamineeDao extends BaseDao
    {
        
        public function insert ($obj)
        {
            
            $conn=$this->getConnection();
            
            
            $sql = "INSERT into examinee (ename,email,phone) 
                     Values('".$obj->getEname()."','".$obj->getEmail()."','".$obj->getPhone()."') ";
            
             if ($conn->query($sql) === TRUE) 
            {
                echo "Success";
            } 
            else 
            {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }
        }
        
        public function dyInsert($data){
            
            $conn=$this->getConnection();
            
            $sql = "select COLUMN_NAME,EXTRA from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='".get_class($data)."'";
            $result=$conn->query($sql);
                     $arr  =  array();
                        while($row=$result->fetch_assoc())
                        {
                          $arr[]=$row;
                        }
            //print_r($arr);

            $str="Insert into ".get_class($data);
            $str.='(';
            $count=0;
            $items= array();
            //echo count($arr);
            for($i=0; $i< count($arr) ; $i++)
            {
               if($arr[$i]['EXTRA'] == '')
               {
                if($count == 0)  
                { $str.=$arr[$i]['COLUMN_NAME']; 

                    $count=1;
                }
                else
                    $str.=','.$arr[$i]['COLUMN_NAME']; 
                $method='get'.ucwords($arr[$i]['COLUMN_NAME']); 
                $value=$data->$method(); 
                $items[]=$value;   
               }

            }

            $str.=")";
            $str.= ' values(';

            for ($i=0;$i<count($items);$i++ )
            {

                 if ($i==0)
                 {
                     $str.="'".$items[$i]."'";
                 }
                else
                    $str.=",'".$items[$i]."'";

            }
            $str.=")";
            //echo $str;
            
             if ($conn->query($str) === TRUE) 
            {
                echo "Success";
            } 
            else 
            {
                echo "Error: " . $str . "<br>" . $conn->error;
            }
        }
        
        public function select ($id)
        {
            $conn= $this->getConnection();
            $sql1 = "select * from examinee where eid='".$id."'";
            $result = $conn->query($sql1);
            $arr =  array();

            if($result->num_rows>0)
            {
                while($row=$result->fetch_assoc())
                {
                  $arr[]=$row;
                }
            }
             echo json_encode($arr);
        }
        
        public function delete ($id)
        {
           $conn= $this->getConnection();
            $sql2= "Delete from examinee where eid='".$id."'";
            
            $result=$conn->query($sql2); 
            if ($conn->query($sql2) === TRUE) 
            {
                echo "Record deleted successfully";
            } 
            else 
            {
                echo "Error deleting record: " . $conn->error;
            }
        }
        public function update($obj,$id)
        {
            $conn= $this->getConnection();
            $sql= " UPDATE examinee
                   SET ename ='".$obj->getEname()."', email ='".$obj->getEmail()."'
                   where eid = '".$id."'";
            //echo $sql;
             $conn->query($sql);
             if ($conn->affected_rows == 1)
            {
                echo "Update successful";
                
            }
            else  
            {
                echo "No such record exists!";
                
            }
        }
        public function dyUpdate($table,$key,$update)
        {
           $conn= $this->getConnection();
            $str="UPDATE ".$table;
            $str.=" SET";
            $count=0;
            foreach ($update as $id => $value) 
            {
                $str.=" ".$id." = '".$value."'";
                $count++;
                if ($count < count($update))
                {
                    $str.= ",";
                }
            }
            $count=0;
            $str.= " where";
            foreach ($key as $id => $value) 
            {
                $str.=" ".$id." = '".$value."'";
                $count++;
                if ($count < count($key))
                {
                    $str.= " and";
                } 
            }
            $conn->query($str);
            if ($conn->affected_rows == 1)
            {
                echo "Update successful";
            }
            else  
            {
                echo "No such record exists!";
            }
        }
        
        public function dySelect($table,$retrieve,$key,$operator)
        {
            $conn= $this->getConnection();
            $length = count($retrieve);
            $str ="SELECT ";
            for($x = 0; $x < $length; $x++)
            {
                if ($x == 0) 
                $str.= " ".$retrieve[$x];
                else
                $str.= ",".$retrieve[$x];
            }
            $str.= " from $table where ";
            $count=0;
            foreach ($key as $id => $value) 
            {
                 $str.=" ".$id." ".$operator[$count]."'".$value."'";
                 $count++;
                if ($count < count($key))
                {
                    $str.= " and";
                } 
            }
            $result=$conn->query($str);
            $arr = array();
            if($result->num_rows>0)
            {
                while($row=$result->fetch_assoc())
                {
                  $arr[]=$row;
                }
            }
            
             echo json_encode($arr); 
        }
    }
?>