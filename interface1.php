<?php
interface Test 
{
public function setVal ($value);
public function getVal ();
}


class Child implements Test

{
    var $var1;
    public function setval($value)
    {

        $this->var1= $value;        

    }
    public function getVal()
{

    echo $this->var1;
}
    
 
}

 abstract class Test2 
{
    var $var2;
    public  function setVal($value)
    {
     }
   
     public abstract function getVal();
    
}

class Child2 extends Test2
{
    
    public function setVal($value)
    {
        $this->var2 = $value;
    
    
    }
    public function getVal()
    {
        echo $this->var2;
        
    }
    
    
}

$t1 = new Child();
$t1->setVal(300);
$t1->getVal();

$t2 =  new Child2();
$t2->setVal(1000);
echo "\n";
$t2->getVal();



?>