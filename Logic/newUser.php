<?php
    
require_once('./Dao/examineeDao.php');
class NewUser 
{
	private $edao;
	public function __construct()
	{
		$this->edao = new ExamineeDao();
		
	}
    public function createuser($obj)
    { 
        
        return  $this->edao->insert($obj);
    }
    
    public function searchUser ($id)
    {
       
        return  $this->$edao->select($id);
    }
    public function deleteUser($id)
    {
      
      return  $this->$edao->delete($id);
    }
    public function updateUser($obj,$id)
    {
        
        return  $this->$edao->update($obj,$id);
    }
     public function dyUpdateUser($table,$key,$update)
    {
        
        return  $this->$edao->dyUpdate($table,$key,$update);
    }
    public function dySelectUser($table,$retrieve,$key,$operator)
    {
        
        return  $this->$edao->dySelect($table,$retrieve,$key,$operator);
    
    }
    public function dyInsertUser($obj)
    {
         return $this->edao->dyInsert($obj);
        
        
    }
}


?>
