<?php
class examinee {
    var $eid;
    var $ename;
    var $email;
    var $phone;
    
    public function setEid($eid) {
        $this->eid = $eid;
    }
    
    public function getEid() {
        return $this->eid;
    }
    
    public function setEname($ename) {
        $this->ename = $ename;
    }
    
    public function getEname() {
        return $this->ename;
    }
    
    public function setEmail($email) {
        $this->email = $email;
    }
    
    public function getEmail() {
        return $this->email;
    }
     public function setPhone($phone) {
        $this->phone = $phone;
    }
    
    public function getPhone() {
        return $this->phone;
    }
}
?>